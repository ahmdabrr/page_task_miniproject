import React, { Component } from 'react';
import { Navbar, Nav,   } from 'react-bootstrap';
import './navbar.css';



class NavBar extends Component {
    render() {
        return (
            <Navbar className="container">
                <Navbar.Brand className="navbar-brand">
                <img
                    alt="logo"
                    src="/assets/images/logo-kb.png"
                    width="184"
                    height="54"                    
                />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse  >
                    <Nav className="menu-nav col-11 " fill variant="pills" activeKey="news">
                        <Nav.Item>
                            <Nav.Link eventKey="home">Beranda</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link eventKey="about">Tentang Kami</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link eventKey="donation">Program Donasi</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link eventKey="news">Berita</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link eventKey="profile">Profile User</Nav.Link>
                        </Nav.Item>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

export default NavBar;