import React, { Component } from 'react';
import './sortir2.css';


class MenuSortir extends Component {
    render() {
        return(
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="text-right justify-content-end">
                            urutkan
                            <p /> 
                            <div class="dropdown">
                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Sortir menurut
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#/">Relevansi</a>
                                    <a class="dropdown-item" href="#/">Tanggal diterbitkan</a>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default MenuSortir;