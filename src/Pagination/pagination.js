import React from 'react';
import './pagination.css';



const Paging = () => {
    return (        
        <div class="pagination justify-content-center">
            <a href="#/">Sebelumnya</a>
            <a class="active" href="#/">1</a>
            <a href="#/">2</a>
            <a href="#/">3</a>
            <a href="#/">4</a>
            <a href="#/">5</a>
            <a href="#/">...</a>
            <a href="#/">150</a>
            <a href="#/">Selanjutnya</a>
        </div>
    );
}

export default Paging;