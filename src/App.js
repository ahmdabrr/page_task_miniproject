import React from 'react';
import './App.css';
import NavBar from './component/NavBar/navbar';
// import Footer from './component/Footer/footer';
// import Konten from './component/Konten/konten2';
// import Paging from './component/Pagination/pagination';
// import Seacrh from './component/Searchbar/seacrh';
// import HeadingBar from './component/Heading Bar/headingbar';
// import SortirMenu from './component/Sortir/sortir2';
// import Profile from './component/Konten/profile';
import Produk from './component/Konten/produk';


function App() {
  return (
    
    <div className="App">
        <NavBar />
        <br /> <br />
        <Produk /> 
        {/* <Footer /> */}
    </div>
  );
}

export default App;