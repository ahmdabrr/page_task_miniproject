import React, { Component } from 'react';
import './NewsCard.css';
import Berbagi from '../assets/images/berbagi.png';
import Sembako from '../assets/images/sembako.png';
import Covid from '../assets/images/covid.png';
import Blusukan from '../assets/images/blusukan.png';
import Akustik from '../assets/images/akustik.png';
import Makanan from '../assets/images/makanan.png';

class KontenCards extends Component {
    render() {
        return(
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <img src={Berbagi} class="card-img-top" alt="..."></img>
                            <div class="card-body">
                                <p class="card-title">Di tengah Pandemi, Ketjilbergerak berbagi kotak nasi untuk Tunawisma</p>
                                <p class="card-text">Ramadan tahun ini jelas beda. Masa pandemi membuat semua lini kehidupan jadi lebih sulit dari biasanya. Termasuk efek kesulitan untuk mencukupi kebutuhannya.</p>
                                <p class="card-title font-footer">8 April 2020</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                    <div class="card">
                            <img src={Sembako} class="card-img-top" alt="..."></img>
                            <div class="card-body">
                                <p class="card-title">Tebar 1.000 paket sembako untuk kelompok rentan</p>
                                <p class="card-text">PT. Super  salurkan sembako ke masyarakat kelompok rentan sebanyak 1.000 paket sembako ke sejumlah wilayah di Yogyakarta.</p>
                                <p class="card-title">15 Maret 2020</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                    <div class="card">
                            <img src={Covid} class="card-img-top" alt="..."></img>
                            <div class="card-body">
                                <p class="card-title">Distribusi paket makanan untuk tim medis covid-19</p>
                                <p class="card-text">Ramadan tahun ini jelas beda. Masa pandemi membuat semua lini kehidupan jadi lebih sulit dari biasanya. Termasuk efek kesulitan untuk mencukupi kebutuhannya.</p>
                                <p class="card-title">12 Maret 2020</p>
                            </div>
                        </div>
                    </div> 
                </div>
                <br /> <br />
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <img src={Blusukan} class="card-img-top" alt="..."></img>
                            <div class="card-body">
                                <p class="card-title">Blusukan, antarkan paket kebahagiaan untuk Tunawisma</p>
                                <p class="card-text">Berpayung mendung, tim dinas sosial Yogyakarta tetap semangat membagikan paket sembako kepada Tunawisma.</p>
                                <p class="card-title">10 Maret 2020</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                    <div class="card">
                            <img src={Akustik} class="card-img-top" alt="..."></img>
                            <div class="card-body">
                                <p class="card-title">Di tengah Pandemi, Ketjilbergerak berbagi kotak nasi untuk Tunawisma</p>
                                <p class="card-text">Ramadan tahun ini jelas beda. Masa pandemi membuat semua lini kehidupan jadi lebih sulit dari biasanya. Termasuk efek kesulitan untuk mencukupi kebutuhannya.</p>
                                <p class="card-title">5 Maret 2020</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                    <div class="card">
                            <img src={Makanan} class="card-img-top" alt="..."></img>
                            <div class="card-body">
                                <p class="card-title">Berbagi makanan sehat untuk anak jalanan</p>
                                <p class="card-text">Makanan sehat adalah hak semua orang. Makan makanan sehat memberikan energi yang baik untuk tubuh. Energi baik mendorong lahirnya pikiran-pikiran baik.</p>
                                <p class="card-title">9 Maret 2020</p>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        );
    }
} 

export default KontenCards;