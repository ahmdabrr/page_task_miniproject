import React, { Component } from 'react';
import './produk.css';
import Produk0 from '../assets/images/produk0.png';
import Produk1 from '../assets/images/produk1.png';
import Produk2 from '../assets/images/produk2.png';
import Produk3 from '../assets/images/produk3.png';
import Produk4 from '../assets/images/produk4.png';
import Produk5 from '../assets/images/produk5.png';
import Produk6 from '../assets/images/produk6.png';

class Produk extends Component {
    render() {
        return(
            <div class="container">
                <div class="row">
                    <div class="col-7 images">
                        <img src={Produk0} class="card-img-top" alt="..."/>
                        <a href="#/"><img src={Produk1} class="img-thumbnail" alt="..."/></a>
                        <a href="#/"><img src={Produk2} class="img-thumbnail-kedua" alt="..."/></a>
                        <a href="#/"><img src={Produk3} class="img-thumbnail-kedua" alt="..."/></a>
                        <a href="#/"><img src={Produk4} class="img-thumbnail-kedua" alt="..."/></a>
                        <a href="#/"><img src={Produk5} class="img-thumbnail-kedua" alt="..."/></a>
                        <a href="#/"><img src={Produk6} class="img-thumbnail-kedua" alt="..."/></a>
                    </div>
                    <div class="col-5">
                        <h3>Makanan berbuka dan sahur untuk Tunawisma</h3>
                        <h4 class="badge-text"><span class="badge">Sambung Rasa</span></h4>
                        <p><i class="material-icons">place</i>Sleman</p>
                        <div class="card">
                            <p class="text-donasi">Dana terkumpul</p>
                            <p class="duit-donasi">Rp.4.000.000</p>
                            <hr />
                            <div class="row">
                            <div class="col-5">
                            <p class="waktu">Waktu</p><h5 class="text-hari">25 hari lagi</h5></div>
                            <div class="col-7 text-right">
                            <p class="partisipan">Berpartisipasi</p><h5 class="text-jumlah">388</h5></div>
                            </div>                            
                        </div>
                        <button type="button" class="btn btn-danger">DONASI</button>
                        <button type="button" class="button-share"><i class="material-icons">share</i>   BAGIKAN</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Produk;