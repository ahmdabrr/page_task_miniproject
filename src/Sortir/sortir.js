import React from 'react';
import {Container, Row, Col, DropdownButton, Dropdown} from 'react-bootstrap';

const SortirMenu = () => {
    return(
        <Container>
            <Row>
            <Col md={4}></Col>
                <Col md={{span:4, offset:4}}>
                    <div className="text-right justify-content-end">
                        urutkan
                        <p /><DropdownButton
                                alignRight
                                title="Sortir menurut"
                                id="dropdown-menu-align-right">
                                <Dropdown.Item eventKey="1">Relevansi</Dropdown.Item>
                                <Dropdown.Item eventKey="2">Tanggal diterbitkan</Dropdown.Item>             
                            </DropdownButton>
                    </div>
                </Col>
            </Row>
        </Container>
    );
}

export default SortirMenu;