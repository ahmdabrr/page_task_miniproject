import React from 'react';
import './footer.css';
import { Row, Col, Container, } from "react-bootstrap";
import TwitterLogo from '../assets/images/twitter.png';
import FBLogo from '../assets/images/facebook.png';
import IGLogo from '../assets/images/instagram.png';
import KB from '../assets/images/logo-kb-white.png';
import MessageIcon from '../assets/images/message.png';



const Footer = () => {
    return(
        <div className="footer">
            <div className="container">
                <div className="row justify-content-between align-content-stretch my-row">
                    {/* column 1 */}
                    <div className="col-lg">
                        <img 
                            alt=""
                            src={KB} 
                            width="198"
                            height="88"
                            />
                            <br />
                        <p>Ketjilbergerak adalah komunitas berbasis anak muda yang bergerak melalui berbagai praktik kerja seni, sosial dan budaya yang bersifat kolaboratif.</p>
                        <br />
                        <div>
                            <img alt="icon" src={MessageIcon}/>
                            
                            <p>ketjilbergerak@support.com</p>
                            
                        </div>
                        <p>0274-741-197</p>
                    </div>
                            <div className="col-lg update">
                            <h5 className="title">Ikuti update kami</h5>
                            <ul>
                            <li className="list-unstyled">
                                <a href="#!"><img alt="" src={TwitterLogo} width="41.8" height="32" />   </a>
                                <a href="#!"><img alt="" src={FBLogo} width="33.9" height="32" />   </a>
                                <a href="#!"><img alt="" src={IGLogo} width="33.9" height="32" />  </a>
                            </li>
                            </ul>
                        </div>
                </div>                     
            </div>
            <div className="footer-bottom" >
                    <Container Fluid>
                    <Row className="justify-content-md-center">
                        <Col xl="auto">&copy;{new Date().getFullYear()} Ketjilbergerak. All right reserved.</Col>
                    </Row>
                    </Container>
                </div>
        </div>
    
    );
}
export default Footer;

