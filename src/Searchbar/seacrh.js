import React from 'react';
import {Button} from 'react-bootstrap'

const Search = () => {
    return (
    <div className="container">
        <div className="row justify-content-end">
        <div className="wrap">
            <div className="search">
                <input type="text" className="searchTerm" placeholder="What are you looking for?"></input>
                &#160; &#160; <Button variant="info">Cari</Button>
            </div>
            </div>
        </div>
    </div>
    );
}

export default Search;