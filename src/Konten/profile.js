import React, { Component } from 'react';
import './profile.css'
import ProfilUser from '../assets/images/profile.png';


class Profile extends Component {
    render() {
        return(
            <div class="container">              
                <div class="card">
                    <img src={ProfilUser} class="img-fluid" alt="..."/>
                    <h1 class="text-profile">Tinky Wingky</h1>
                    <p class="text1">@tante_ernie | tingkywingky@gmail.com</p>
                    <p class="text2">Follow instagram aku @hymynameisernie</p>
                    <p class="text3">Jangan lupa subscribe, like comment dan bunyikan lonceng notifikasi</p>
                </div>    
            </div>
        );
    }
}

export default Profile;